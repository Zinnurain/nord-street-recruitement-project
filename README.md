# nord-street-recruitement-project

# Used tools

Nginx, PHP 8.2, MySQL 8, RabbitMQ, Redis and Symfony 6.2 framework

# Project setup
 - Clone this project
 - Create docker image and container from docker compose file.
 - composer Install
 - Install ChromeDriver (if not available). Set the ChromeDriver path into .env file


## Project description
This project has scraped data "Company profile" CRUD, scraped data from source. This source has CRUD.
